package br.com.restapi.tdd.config;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import br.com.restapi.tdd.domain.User;
import br.com.restapi.tdd.repositories.UserRepository;

@Configuration
@Profile("local")
public class LocalConfig {
	
	@Autowired
	private UserRepository repository;
	
	@Bean
	public void startDB() {
		User u1 = new User(null, "william", "william.teste@gmail.com", "123");
		User u2 = new User(null, "luiz", "luiz.teste@gmail.com", "321");
		
		repository.saveAll(List.of(u1,u2));
	}
}
