package br.com.restapi.tdd.services.impl;

import java.util.List;
import java.util.Optional;

import br.com.restapi.tdd.services.exceptions.DataIntegrityViolationException;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import br.com.restapi.tdd.domain.User;
import br.com.restapi.tdd.domain.dto.UserDTO;
import br.com.restapi.tdd.repositories.UserRepository;
import br.com.restapi.tdd.services.UserService;
import br.com.restapi.tdd.services.exceptions.ObjectNotFoundException;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

	private UserRepository repository;
	private ModelMapper mapper;
	
	@Override
	public User findById(Integer id) {
		Optional<User> obj = repository.findById(id);
		return obj.orElseThrow(() -> new ObjectNotFoundException("Objeto não encontrado"));
	}

	@Override
	public List<User> findAll() { return repository.findAll(); }

	@Override
	public User create(UserDTO obj) {
		this.findByEmail(obj);
		return repository.save(mapper.map(obj, User.class));
	}

	@Override
	public User update(UserDTO obj) {
		this.findByEmail(obj);
		return repository.save(mapper.map(obj, User.class));
	}

	@Override
	public void delete(Integer id) {
		this.findById(id);
		repository.deleteById(id);
	}

	private void findByEmail(UserDTO obj) {
		Optional<User> user = repository.findByEmail(obj.getEmail());
		if(user.isPresent() && !user.get().getId().equals(obj.getId()))
			throw new DataIntegrityViolationException("Email já cadastrado!");
	}
}
