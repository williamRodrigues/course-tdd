package br.com.restapi.tdd.resources;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import br.com.restapi.tdd.domain.User;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.restapi.tdd.domain.dto.UserDTO;
import br.com.restapi.tdd.services.impl.UserServiceImpl;

@RestController
@RequestMapping(value = "/user")
@AllArgsConstructor
public class UserResource {

	public static final String ID = "/{id}";
	private ModelMapper mapper;
	
	private UserServiceImpl service;
	
	@GetMapping(value = ID)
	public ResponseEntity<UserDTO> findById(@PathVariable Integer id) {
		return ResponseEntity.ok().body(mapper.map(service.findById(id), UserDTO.class));
	}
	
	@GetMapping
	public ResponseEntity<List<UserDTO>> findAll() {
		return ResponseEntity.ok()
				.body(service.findAll()
						.stream().map(user -> mapper.map(user, UserDTO.class)).collect(Collectors.toList()));
	}
	
	@PostMapping
	public ResponseEntity<UserDTO> create(@RequestBody UserDTO obj) {
		URI uri = ServletUriComponentsBuilder
				.fromCurrentRequest()
				.path(ID)
				.buildAndExpand(service.create(obj).getId()).toUri();
		return ResponseEntity.created(uri).build();
	}

	@PutMapping(value = ID)
	public ResponseEntity<UserDTO> update(@PathVariable Integer id, @RequestBody UserDTO obj) {
		obj.setId(id);
		User newObj = service.update(obj);
		return ResponseEntity.ok().body(mapper.map(newObj, UserDTO.class));
	}

	@DeleteMapping(value = ID)
	public ResponseEntity<UserDTO> delete(@PathVariable Integer id) {
			service.delete(id);
			return ResponseEntity.noContent().build();
	}
}
