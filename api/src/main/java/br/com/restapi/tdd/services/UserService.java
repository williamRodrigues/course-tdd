package br.com.restapi.tdd.services;

import java.util.List;

import br.com.restapi.tdd.domain.User;
import br.com.restapi.tdd.domain.dto.UserDTO;

public interface UserService {

	User findById(Integer id);
	
	List<User> findAll();
	
	User create(UserDTO obj);

	User update(UserDTO obj);

	void delete(Integer id);
}
